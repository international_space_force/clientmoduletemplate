#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include "Practical.h"

#include "mavlink.h"
#include <queue>

#define BUFFER_LENGTH 2041 // minimum buffer size that can be used with qnx (I don't know why)

int main(int argc, char* argv[]) {
    if(argc < 3 || argc > 4) {
        DieWithUserMessage("Parameter(s)", "<Server Address/Name> <Echo Word> [<Server Port/Service>]");
    }
    
    char* server = argv[1];
    char* echoString = argv[2];
    
    size_t echoStringLen = strlen(echoString);
    if(echoStringLen > MAXSTRINGLENGTH) {
        DieWithUserMessage(echoString, "string too long");
    }
    
    const char* serverPort = (argc == 4) ? argv[3] : "echo";
    
    // Tell the system what kidns of address info we want
    // Criteria for address match
    struct addrinfo addrCriteria;
    // Zero out structure
    memset(&addrCriteria, 0, sizeof(addrCriteria));
    // Any address family
    addrCriteria.ai_family = AF_UNSPEC;
    
    // For the following fields, a zero value means don't care
    // Only data sockets
    addrCriteria.ai_socktype = SOCK_DGRAM;
    // Only UDP protocol
    addrCriteria.ai_protocol = IPPROTO_UDP;
    
    // Get address(es)
    struct addrinfo* serverAddress;
    int returnValue = getaddrinfo(server, serverPort, &addrCriteria, &serverAddress);
    if(returnValue != 0) {
        DieWithUserMessage("getaddrinfo() failed", gai_strerror(returnValue));
    }
    
    // Create a datagram/UDP socket
    // Socket descriptor for client
    int sock = socket(serverAddress->ai_family, serverAddress->ai_socktype, serverAddress->ai_protocol);
    if(sock < 0) {
        DieWithSystemMessage("socket() failed");
    }
    
    char loopControl = 's';
    do {
        /* Send Heartbeat */
        uint8_t buf[BUFFER_LENGTH];
        mavlink_message_t msg;
        uint16_t len;
        mavlink_msg_heartbeat_pack(1, 200, &msg, MAV_TYPE_GROUND_ROVER, MAV_AUTOPILOT_GENERIC, MAV_MODE_GUIDED_ARMED, 0, MAV_STATE_ACTIVE);
        len = mavlink_msg_to_send_buffer(buf, &msg);
        ssize_t numBytes = sendto(sock, &buf, len, 0, serverAddress->ai_addr, serverAddress->ai_addrlen);
        
        // Send a string to the server
        //ssize_t numBytes = sendto(sock, echoString, echoStringLen, 0, serverAddress->ai_addr, serverAddress->ai_addrlen);
        if(numBytes < 0) {
            DieWithSystemMessage("sendto() failed");
        } else if(numBytes != len) {
            DieWithUserMessage("sendto() error", "sent unexpected number of bytes");
        }
        
        // Receive a response
        // Source address of server
        struct sockaddr_storage fromAddr;
        // Set length of from address structure (in-out parameter)
        socklen_t fromAddrLen = sizeof(fromAddr);
        // I/O buffer
        char buffer[MAXSTRINGLENGTH + 1];
        printf("Attempting to get response from server\n");
        //numBytes = recvfrom(sock, buffer, MAXSTRINGLENGTH, 0, (struct sockaddr*)&fromAddr, &fromAddrLen);
        memset(buf, 0, BUFFER_LENGTH);
        numBytes = recvfrom(sock, (void *)buf, BUFFER_LENGTH, 0, (struct sockaddr*)&fromAddr, &fromAddrLen);
        if(numBytes < 0) {
            DieWithSystemMessage("recvfrom() failed");
        } else if(numBytes != len) {
            DieWithUserMessage("recvfrom() error", "received unexpected number of bytes");
        }
        
        // Something received - print out all bytes and parse packet
        mavlink_status_t status;
        
        printf("Bytes Received: %d\nDatagram: ", (int)numBytes);
        for (int i = 0; i < numBytes; ++i)
        {
            unsigned int temp = 0;
            temp = buf[i];
            printf("%02x ", (unsigned char)temp);
            if (mavlink_parse_char(MAVLINK_COMM_0, buf[i], &msg, &status))
            {
                // Packet received
                printf("\nReceived packet: SYS: %d, COMP: %d, LEN: %d, MSG ID: %d\n", msg.sysid, msg.compid, msg.len, msg.msgid);
            }
        }
        printf("\n");
        
        // Verify reception from expected source
        if(!SockAddrsEqual(serverAddress->ai_addr, (struct sockaddr*)&fromAddr)) {
            DieWithUserMessage("recvfrom() error", "received a packet from unknown source");
        }
        
        // Null terminate received data
        buffer[echoStringLen] = '\0';
        printf("Received: %s\n", buffer);
        
        // Send collision message
        printf("Attempting to send collision message\n");
        mavlink_msg_collision_pack(1, 190, &msg, MAV_TYPE_GROUND_ROVER, MAV_AUTOPILOT_GENERIC, MAV_COLLISION_ACTION_HOVER, MAV_COLLISION_THREAT_LEVEL_LOW, 1.2, 0.0, 2.0);
        len = mavlink_msg_to_send_buffer(buf, &msg);
        numBytes = sendto(sock, &buf, len, 0, serverAddress->ai_addr, serverAddress->ai_addrlen);
        
        // Send a string to the server
        //ssize_t numBytes = sendto(sock, echoString, echoStringLen, 0, serverAddress->ai_addr, serverAddress->ai_addrlen);
        if(numBytes < 0) {
            DieWithSystemMessage("sendto() failed");
        } else if(numBytes != len) {
            DieWithUserMessage("sendto() error", "sent unexpected number of bytes");
        }
        
        // Set length of from address structure (in-out parameter)
        fromAddrLen = sizeof(fromAddr);

        printf("Attempting to get response from server\n");
        //numBytes = recvfrom(sock, buffer, MAXSTRINGLENGTH, 0, (struct sockaddr*)&fromAddr, &fromAddrLen);
        memset(buf, 0, BUFFER_LENGTH);
        numBytes = recvfrom(sock, (void *)buf, BUFFER_LENGTH, 0, (struct sockaddr*)&fromAddr, &fromAddrLen);
        if(numBytes < 0) {
            DieWithSystemMessage("recvfrom() failed");
        } else if(numBytes != len) {
            DieWithUserMessage("recvfrom() error", "received unexpected number of bytes");
        }
        
        printf("Bytes Received: %d\nDatagram: ", (int)numBytes);
        for (int i = 0; i < numBytes; ++i)
        {
            unsigned int temp = 0;
            temp = buf[i];
            printf("%02x ", (unsigned char)temp);
            if (mavlink_parse_char(MAVLINK_COMM_0, buf[i], &msg, &status))
            {
                // Packet received
                printf("\nReceived packet: SYS: %d, COMP: %d, LEN: %d, MSG ID: %d\n", msg.sysid, msg.compid, msg.len, msg.msgid);
            }
        }
        printf("\n");
        
        // Verify reception from expected source
        if(!SockAddrsEqual(serverAddress->ai_addr, (struct sockaddr*)&fromAddr)) {
            DieWithUserMessage("recvfrom() error", "received a packet from unknown source");
        }
        
        // Null terminate received data
        buffer[echoStringLen] = '\0';
        printf("Received: %s\n", buffer);
        
        printf("Enter s to stop\n");
        loopControl = (char)getchar();
    } while(loopControl != 's');
        
    freeaddrinfo(serverAddress);
    close(sock);
    
    return 0;
}
